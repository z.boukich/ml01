from enum import Enum

NUMBEROFDOCUMENTS = 90302
MISSINGVALUE = '-999999'

class CPTParams(Enum):
    PENETRATIONLENGTH = (0, "sondeertrajectlengte")
    DEPTH = (1, "diepte")
    ELAPSEDTIME = (2, "verlopen tijd")
    CONERESISTANCE = (3, "conusweerstand")
    CORRECTEDCONERESISTANCE = (4, "gecorrigeerde conusweerstand")
    NETCONERESISTANCE = (5, "netto conusweerstand")
    MAGNETICFIELDSTRENGTHX = (6, "magnetische veldsterkte x")
    MAGNETICFIELDSTRENGTHY = (7, "magnetische veldsterkte y")
    MAGNETICFIELDSTRENGTHZ = (8, "magnetische veldsterkte z")
    MAGNETICFIELDSTRENGTHTOTAL = (9, "totale magnetische veldsterkte")
    ELECTRICALCONDUCTIVITY = (10, "elektrische geleidbaarheid")
    INCLINATIONEW = (11, "helling oost-west")
    INCLINATIONNS = (12, "helling noord-zuid")
    INCLINATIONX = (13, "helling x")
    INCLINATIONY = (14, "helling y")
    INCLINATIONRESULTANT = (15, "hellingresultante")
    MAGNETICINCLINATION = (16, "magnetische inclinatie")
    MAGNETICDECLINATION = (17, "magnetische declinatie")
    LOCALFRICTION = (18, "plaatselijke wrijving")
    PORERATIO = (19, "porienratio")
    TEMPERATURE = (20, "temperatuur")
    POREPRESSUREU1 = (21, "waterspanning u1")
    POREPRESSUREU2 = (22, "waterspanning u2")
    POREPRESSUREU3 = (23, "waterspanning u3")
    FRICTIONRATIO = (24, "wrijvingsgetal")
