from flask import Flask, request
from sklearn.externals import joblib
import numpy as np
import json
import os

app = Flask(__name__)
dirname = os.path.abspath(os.curdir)

NEURAL_NETWORK = "neural_network"
models = {}

devEnv = __name__ == "__main__"
if (devEnv): 
    os.chdir("..")
    app.config["ENV"] = "development"

def loadModel(name):
    if (devEnv):
        model = joblib.load(dirname + "/finalized/finalized_model_" + name + ".sav")
    else:
        model = joblib.load("/finalized/finalized_model_" + name + ".sav")
    models[name] = model

def bootstrap():
    print("bootstrapping")
    loadModel(NEURAL_NETWORK)
    print("bootstrap completed.")

# GO!
bootstrap()

# The request method is POST (this method enables us to send CPT measures to the endpoint in the request body and printing the prediction)
@app.route('/', methods=["POST"])
def evaluate():
    data = {}
    body = request.get_json()
    if 'ml' in body:
        ml = body['ml']
        array_values = np.asarray(list(ml.values())).reshape(1, -1)
        for predictor in models:
            pred = models[predictor].predict(array_values)
            predictoryMap = {}
            predictoryMap['predicted_label'] = pred[0]
            data[predictor] = predictoryMap
        return json.dumps(data)
       
if devEnv:
    print("* Starting web server... please wait until server has fully started")
    app.run(host='0.0.0.0', port=4000, threaded=False)
