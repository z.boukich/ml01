from utils.enums import NUMBEROFDOCUMENTS
from scripts.broparser import BROXMLParser
from scripts.brovalidation import assertDirectoryExists
import os
import psycopg2

'''
     Tables 
'''
'''
  Entiteit Geotechnisch onderzoek
'''
def geotechnisch_sondeeronderzoek():
    cur.execute('''CREATE TABLE Geotechnischsondeeronderzoek
      (fileid bigint NOT NULL,
      broid CHARACTER VARYING(100),
      bronhouder CHARACTER VARYING(100),
      objectid_bronhouder CHARACTER VARYING(100),
      dataleverancier CHARACTER VARYING(100),
      kwaliteitsregime CHARACTER VARYING(100),
      kader_aanlevering CHARACTER VARYING(100),
      kader_inwinning CHARACTER VARYING(100),
      rapportagedatum CHARACTER VARYING(100),
      sondeernorm CHARACTER VARYING(100),
      aanvullend_onderzoek_uitgevoerd CHARACTER VARYING(100),
      uitvoerder_onderzoek CHARACTER VARYING(100)
      );''')
    print ("Table Geotechnisch sondeeronderzoek created successfully")

'''
  Entiteit Registratiegeschiedenis
'''
def registratiegeschiedenis():
    cur.execute('''CREATE TABLE Registratiegeschiedenis
      (fileid bigint NOT NULL,
      tijdstip_registratie_object CHARACTER VARYING(100),
      registratiestatus CHARACTER VARYING(100),
      tijdstip_voltooiing_registratie CHARACTER VARYING(100),
      gecorrigeerd CHARACTER VARYING(10),
      tijdstip_laatste_correctie CHARACTER VARYING(100),
      in_onderzoek CHARACTER VARYING(10),
      in_onderzoek_sinds CHARACTER VARYING(100),
      uit_registratie_genomen CHARACTER VARYING(10),
      tijdstip_uit_registratie_genomen CHARACTER VARYING(100),
      weer_in_registratie_genomen CHARACTER VARYING(10),
      tijdstip_weer_in_registratie_genomen CHARACTER VARYING(100),
      );''')
    print ("Table Registratiegeschiedenis created successfully")

'''
  Entiteit Aangeleverde locatie
'''
def aangeleverde_locatie():
    cur.execute('''CREATE TABLE Aangeleverdelocatie
      (fileid bigint NOT NULL,
      x double precision,
      y double precision,
      referentiestelsel CHARACTER VARYING(100),
      datum_locatiebepaling CHARACTER VARYING(100),
      methode_locatiebepaling CHARACTER VARYING(100),
      uitvoerder_locatiebepaling CHARACTER VARYING(100)
      );''')
    print ("Table Aangeleverde locatie created successfully")

'''
  Entiteit Aangeleverde verticale locatie
'''
def aangeleverde_verticale_locatie():
    cur.execute('''CREATE TABLE AangeleverdeVerticalelocatie
      (fileid bigint NOT NULL,
      lokaal_verticaal_referentiepunt CHARACTER VARYING(100),
      verschuiving double precision,
      waterdiepte double precision,
      verticaal_referentievlak CHARACTER VARYING(100),
      datum_verticale_positiebepaling CHARACTER VARYING(100),
      methode_verticale_positiebepaling CHARACTER VARYING(100),
      uitvoerder_verticale_positiebepaling CHARACTER VARYING(100)
      );''')
    print ("Table Aangeleverde verticale locatie created successfully")

'''
  Entiteit Gestandaardiseerde locatie
'''
def gestandaardiseerde_locatie():
    cur.execute('''CREATE TABLE Gestandaardiseerdelocatie
      (fileid bigint NOT NULL,
      x double precision,
      y double precision,
      referentiestelsel CHARACTER VARYING(100),
      coordinaattransformatie CHARACTER VARYING(100)
      );''')
    print ("Table Gestandaardiseerde locatie created successfully")

'''
  Entiteit Sondeonderzoek 
'''
def sondeonderzoek():
  cur.execute('''CREATE TABLE Sondeonderzoek
      (fileid bigint NOT NULL,
      dissipatietest_uitgevoerd CHARACTER VARYING(100),
      datum_laatste_bewerking CHARACTER VARYING(100),
      sondeermethode CHARACTER VARYING(100),
      kwaliteitsklasse CHARACTER VARYING(100),
      stopcriterium CHARACTER VARYING(100),
      sensorazimuth integer
      );''')
  print ("Table Sondeonderzoek created successfully")

'''
  Entiteit Traject 
'''
def traject():
  cur.execute('''CREATE TABLE Traject
      (fileid bigint NOT NULL,
      voorgeboord_tot double precision,
      einddiepte double precision
      );''')
  print ("Table Traject created successfully")

'''
  Entiteit Bewerking 
'''
def bewerking():
  cur.execute('''CREATE TABLE Bewerking
      (fileid bigint NOT NULL,
      bewerking_onderbrekingen_uitgevoerd CHARACTER VARYING(100),
      expertcorrectie_uitgevoerd CHARACTER VARYING(100),
      signaalbewerking_uitgevoerd CHARACTER VARYING(100)
      );''')
  print ("Table Bewerking created successfully")

'''
  Entiteit Sondeerapparaat 
'''
def sondeerapparaat():
  cur.execute('''CREATE TABLE Sondeerapparaat
      (fileid bigint NOT NULL,
      omschrijving CHARACTER VARYING(255),
      conustype CHARACTER VARYING(255),
      oppervlakte_conuspunt integer,
      conusdiameter integer,
      oppervlaktequotient_conuspunt double precision,
      afstand_conus_tot_midden_kleefmantel integer,
      oppervlakte_kleefmantel double precision, 
      oppervlaktequotient_kleefmantel double precision
      );''')
  print ("Table Sondeerapparaat created successfully")

'''
  Entiteit Nulmeting
'''
def nulmeting():
  cur.execute('''CREATE TABLE Nulmeting
      (fileid bigint NOT NULL,
      conusweerstand_vooraf double precision,
      conusweerstand_achteraf double precision,
      elektrische_geleidbaarheid_vooraf double precision,
      elektrische_geleidbaarheid_achteraf double precision,
      helling_oost_west_vooraf integer,
      helling_oost_west_achteraf integer,
      helling_noord_zuid_vooraf integer,
      helling_noord_zuid_achteraf integer,
      hellingresultante_vooraf integer,
      hellingresultante_achteraf integer,
      plaatselijke_wrijving_vooraf double precision,
      plaatselijke_wrijving_achteraf double precision,
      waterspanning_u1_vooraf double precision,
      waterspanning_u1_achteraf double precision,
      waterspanning_u2_vooraf double precision,
      waterspanning_u2_achteraf double precision,
      waterspanning_u3_vooraf double precision,
      waterspanning_u3_achteraf double precision
      );''')
  print ("Table Nulmeting created successfully")

'''
  Entiteit Bepaalde parameters 
'''
def bepaalde_parameters():
  cur.execute('''CREATE TABLE Bepaaldeparameters
      (fileid bigint NOT NULL,
      sondeertrajectlengte CHARACTER VARYING(10),
      diepte CHARACTER VARYING(10),
      verlopen_tijd CHARACTER VARYING(10),
      conusweerstand CHARACTER VARYING(10),
      gecorrigeerde_conusweerstand CHARACTER VARYING(10),
      netto_conusweerstand CHARACTER VARYING(10),
      magnetische_veldsterkte_x CHARACTER VARYING(10), 
      magnetische_veldsterkte_y CHARACTER VARYING(10),
      magnetische_veldsterkte_z CHARACTER VARYING(10),
      totale_magnetische_veldsterkte CHARACTER VARYING(10),
      elektrische_geleidbaarheid CHARACTER VARYING(10),
      helling_oost_west CHARACTER VARYING(10),
      helling_noord_zuid CHARACTER VARYING(10),
      helling_x CHARACTER VARYING(10),
      helling_y CHARACTER VARYING(10),
      hellingresultante CHARACTER VARYING(10),
      magnetische_inclinatie CHARACTER VARYING(10),
      magnetische_declinatie CHARACTER VARYING(10),
      plaatselijke_wrijving CHARACTER VARYING(10),
      porienratio CHARACTER VARYING(10),
      temperatuur CHARACTER VARYING(10),
      waterspanning_u1 CHARACTER VARYING(10),
      waterspanning_u2 CHARACTER VARYING(10),
      waterspanning_u3 CHARACTER VARYING(10),
      wrijvingsgetal CHARACTER VARYING(10)
      );''')
  print ("Table Bepaalde parameters created successfully")

'''
  Entiteit Conuspenetratietest  
'''
def conuspenetratietest():
  cur.execute('''CREATE TABLE Conuspenetratietest
      (fileid bigint NOT NULL,
      starttijd CHARACTER VARYING(100),
      resulttijd CHARACTER VARYING(100),
      cptbroid CHARACTER VARYING(100),
      aantal_metingen integer
      );''')
  print ("Table Conuspenetratietest created successfully")

'''
  Entiteit Dissipatietest 
'''
def dissipatietest():
  cur.execute('''CREATE TABLE Dissipatietest
      (fileid bigint NOT NULL,
      sondeertrajectlengte CHARACTER VARYING(100),
      starttijd CHARACTER VARYING(100),
      resulttijd CHARACTER VARYING(100),
      dtbroid CHARACTER VARYING(100),
      aantal_metingen integer
      );''')
  print ("Table Dissipatietest created successfully")

'''
    Insert operations 
'''

'''
  Entiteit Geotechnisch onderzoek
'''
def fill_geotechnisch_sondeeronderzoek():
  query = '''insert into Geotechnischsondeeronderzoek(fileid, broid, bronhouder, objectid_bronhouder, dataleverancier, kwaliteitsregime, kader_aanlevering, kader_inwinning,
   rapportagedatum, sondeernorm, aanvullend_onderzoek_uitgevoerd, uitvoerder_onderzoek) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
  '''
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
    bro_id = parser.get_element_from_metadata('broId')
    bronhouder = parser.get_element_from_metadata('deliveryAccountableParty')
    objectid_bronhouder = parser.get_element_from_metadata('objectIdAccountableParty')
    dataleverancier = parser.get_element_from_metadata('deliveryResponsibleParty ')
    kwaliteitsregime = parser.get_element_from_metadata('qualityRegime')
    kader_aanlevering = parser.get_element_from_metadata('deliveryContext')
    kader_inwinning = parser.get_element_from_metadata('surveyPurpose')
    rapportagedatum = parser.get_element_from_metadata('researchReportDate')
    sondeernorm = parser.get_element_from_metadata('cptStandard')
    aanvullend_onderzoek_uitgevoerd = parser.get_element_from_metadata('additionalInvestigationPerformed')
    uitvoerder_onderzoek = parser.get_element_from_metadata('researchOperator')
    cur.execute(query, (i, bro_id, bronhouder, objectid_bronhouder, dataleverancier, kwaliteitsregime, kader_aanlevering, kader_inwinning, rapportagedatum, sondeernorm, aanvullend_onderzoek_uitgevoerd, uitvoerder_onderzoek))

'''
  Entiteit Registratiegeschiedenis
'''
def fill_registratiegeschiedenis():
  query = '''insert into Registratiegeschiedenis(fileid, tijdstip_registratie_object, registratiestatus, tijdstip_voltooiing_registratie, gecorrigeerd,
   tijdstip_laatste_correctie, in_onderzoek, in_onderzoek_sinds, uit_registratie_genomen, tijdstip_uit_registratie_genomen, weer_in_registratie_genomen, 
   tijdstip_weer_in_registratie_genomen) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
  '''
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
    tijdstip_registratie_object = parser.get_element_from_metadata('objectRegistrationTime')
    registratiestatus = parser.get_element_from_metadata('registrationStatus')
    tijdstip_voltooiing_registratie = parser.get_element_from_metadata('registrationCompletionTime')
    gecorrigeerd = parser.get_element_from_metadata('corrected')
    tijdstip_laatste_correctie = parser.get_element_from_metadata('latestCorrectionTime')
    in_onderzoek = parser.get_element_from_metadata('underReview')
    in_onderzoek_sinds = parser.get_element_from_metadata('underReviewTime')
    uit_registratie_genomen = parser.get_element_from_metadata('deregistered')
    tijdstip_uit_registratie_genomen = parser.get_element_from_metadata('deregistrationTime')
    weer_in_registratie_genomen = parser.get_element_from_metadata('reregistered')
    tijdstip_weer_in_registratie_genomen  = parser.get_element_from_metadata('reregistrationTime')
    cur.execute(query, (i, tijdstip_registratie_object, registratiestatus, tijdstip_voltooiing_registratie, gecorrigeerd, tijdstip_laatste_correctie, in_onderzoek, in_onderzoek_sinds, uit_registratie_genomen, tijdstip_uit_registratie_genomen, weer_in_registratie_genomen, tijdstip_weer_in_registratie_genomen))

'''
  Entiteit Aangeleverde locatie
'''
def fill_aangeleverde_locatie():
  query = '''insert into Aangeleverdelocatie(fileid, x, y, referentiestelsel, datum_locatiebepaling, methode_locatiebepaling, uitvoerder_locatiebepaling) values(%s, %s, %s, %s, %s, %s, %s)
  '''
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
    location = parser.get_location('deliveredLocation')
    x = location[1]
    y = location[2]
    referentiestelsel = location[0]
    datum = parser.get_element_from_metadata('horizontalPositioningDate')
    methode = parser.get_element_from_metadata('horizontalPositioningMethod')
    uitvoerder = parser.get_element_from_metadata('horizontalPositioningOperator')
    cur.execute(query, (i, x, y, referentiestelsel, datum, methode, uitvoerder))

'''
  Entiteit Aangeleverde verticale locatie
'''
def fill_aangeleverde_verticale_locatie():
  query = '''insert into AangeleverdeVerticalelocatie(fileid, lokaal_verticaal_referentiepunt, verschuiving, waterdiepte,
   verticaal_referentievlak, datum_verticale_positiebepaling, methode_verticale_positiebepaling, uitvoerder_verticale_positiebepaling) values(%s, %s, %s, %s, %s, %s, %s, %s)
  '''
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
    lokaal_verticaal_referentiepunt = parser.get_element_from_metadata('localVerticalReferencePoint')
    verschuiving = parser.get_element_from_metadata('offset')
    waterdiepte = parser.get_element_from_metadata('waterDepth')
    verticaal_referentievlak = parser.get_element_from_metadata('verticalDatum')
    datum_verticale_positiebepaling = parser.get_element_from_metadata('verticalPositioningDate')
    methode_verticale_positiebepaling = parser.get_element_from_metadata('verticalPositioningMethod')
    uitvoerder_verticale_positiebepaling = parser.get_element_from_metadata('verticalPositioningOperator')
    cur.execute(query, (i, lokaal_verticaal_referentiepunt, verschuiving, waterdiepte, verticaal_referentievlak, datum_verticale_positiebepaling, methode_verticale_positiebepaling, uitvoerder_verticale_positiebepaling))

'''
  Entiteit Gestandaardiseerde locatie
'''
def fill_gestandaardiseerde_locatie():
  query = '''insert into Gestandaardiseerdelocatie(fileid, x, y, referentiestelsel, coordinaattransformatie) values(%s, %s, %s, %s, %s)
  '''
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
    location = parser.get_location('standardizedLocation')
    x = location[1]
    y = location[2]
    referentiestelsel = location[0]
    coördinaattransformatie = parser.get_element_from_metadata('coordinateTransformation')
    cur.execute(query, (i, x, y, referentiestelsel, coördinaattransformatie))

'''
  Entiteit Sondeonderzoek 
'''
def fill_sondeonderzoek():
  query = '''insert into Sondeonderzoek(fileid, dissipatietest_uitgevoerd, datum_laatste_bewerking, sondeermethode,
   kwaliteitsklasse, stopcriterium, sensorazimuth) values(%s, %s, %s, %s, %s, %s, %s)
  '''
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
    dissipatietest_uitgevoerd =  parser.get_element_from_metadata('dissipationTestPerformed')
    datum_laatste_bewerking = parser.get_element_from_metadata('finalProcessingDate')
    sondeermethode = parser.get_element_from_metadata('cptMethod')
    kwaliteitsklasse = parser.get_element_from_metadata('qualityClass')
    stopcriterium = parser.get_element_from_metadata('stopCriterion')
    sensorazimuth = parser.get_element_from_metadata('sensorAzimuth')
    if sensorazimuth is not None:
      sensorazimuth = int(sensorazimuth)
    cur.execute(query, (i, dissipatietest_uitgevoerd, datum_laatste_bewerking, sondeermethode, kwaliteitsklasse, stopcriterium, sensorazimuth))

'''
  Entiteit Traject
'''
def fill_traject():
  query = 'insert into Traject(fileid, voorgeboord_tot, einddiepte) values(%s, %s, %s)'
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
    voorgeboord = parser.get_element_from_metadata('predrilledDepth')
    einddiepte = parser.get_element_from_metadata('finalDepth')
    cur.execute(query, (i, voorgeboord, einddiepte))

'''
  Entiteit Bewerking
'''
def fill_bewerking():
  query = 'insert into Bewerking(fileid, bewerking_onderbrekingen_uitgevoerd, expertcorrectie_uitgevoerd, signaalbewerking_uitgevoerd) values(%s, %s, %s, %s)'
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
    bewerking_onderbrekingen_uitgevoerd = parser.get_element_from_metadata('interruptionProcessingPerformed')
    expertcorrectie_uitgevoerd = parser.get_element_from_metadata('expertCorrectionPerformed')
    signaalbewerking_uitgevoerd = parser.get_element_from_metadata('signalProcessingPerformed')
    cur.execute(query, (i, bewerking_onderbrekingen_uitgevoerd, expertcorrectie_uitgevoerd, signaalbewerking_uitgevoerd))

'''
  Entiteit Sondeerapparaat
'''
def fill_sondeerapparaat():
  query = 'insert into Sondeerapparaat(fileid, omschrijving, conustype, oppervlakte_conuspunt, conusdiameter, oppervlaktequotient_conuspunt, afstand_conus_tot_midden_kleefmantel, oppervlakte_kleefmantel, oppervlaktequotient_kleefmantel) values(%s, %s, %s, %s, %s, %s, %s, %s, %s)'
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
    omschrijving = parser.get_element_from_metadata('description')
    conustype = parser.get_element_from_metadata('conePenetrometerType')
    oppervlakte_conuspunt = parser.get_element_from_metadata('coneSurfaceArea')
    conusdiameter = parser.get_element_from_metadata('coneDiameter')
    oppervlaktequotient_conuspunt = parser.get_element_from_metadata('coneSurfaceQuotient') 
    afstand_conus_tot_midden_kleefmantel= parser.get_element_from_metadata('coneToFrictionSleeveDistance') 
    oppervlakte_kleefmantel= parser.get_element_from_metadata('frictionSleeveSurfaceArea') 
    oppervlaktequotient_kleefmantel= parser.get_element_from_metadata('frictionSleeveSurfaceQuotient') 
    cur.execute(query, (i, omschrijving, conustype, oppervlakte_conuspunt, conusdiameter, oppervlaktequotient_conuspunt, afstand_conus_tot_midden_kleefmantel, oppervlakte_kleefmantel, oppervlaktequotient_kleefmantel))

'''
  Entiteit nulmeting
'''
def fill_nulmeting():
  query = '''insert into Nulmeting(fileid, conusweerstand_vooraf, conusweerstand_achteraf, elektrische_geleidbaarheid_vooraf, elektrische_geleidbaarheid_achteraf,
  helling_oost_west_vooraf, helling_oost_west_achteraf, helling_noord_zuid_vooraf, helling_noord_zuid_achteraf, hellingresultante_vooraf, hellingresultante_achteraf,
  plaatselijke_wrijving_vooraf, plaatselijke_wrijving_achteraf, waterspanning_u1_vooraf, waterspanning_u1_achteraf, waterspanning_u2_vooraf, waterspanning_u2_achteraf,
  waterspanning_u3_vooraf, waterspanning_u3_achteraf) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
  '''
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
    nulmeting = parser.get_element_from_metadata('zeroLoadMeasurement')
    if nulmeting is None:
      continue
    conusweerstand_vooraf =  get_ot_none(nulmeting, 'coneResistanceBefore')
    conusweerstand_achteraf = get_ot_none(nulmeting, 'coneResistanceAfter')
    elektrische_geleidbaarheid_vooraf = get_ot_none(nulmeting, 'electricalConductivityBefore')
    elektrische_geleidbaarheid_achteraf = get_ot_none(nulmeting, 'electricalConductivityAfter')
    helling_oost_west_vooraf = get_ot_none(nulmeting, 'inclinationEWBefore')
    helling_oost_west_achteraf = get_ot_none(nulmeting, 'inclinationEWAfter')
    helling_noord_zuid_vooraf = get_ot_none(nulmeting, 'inclinationNSBefore')
    helling_noord_zuid_achteraf = get_ot_none(nulmeting, 'inclinationNSAfter')
    hellingresultante_vooraf = get_ot_none(nulmeting, 'inclinationResultantBefore')
    hellingresultante_achteraf = get_ot_none(nulmeting, 'inclinationResultantAfter')
    plaatselijke_wrijving_vooraf = get_ot_none(nulmeting, 'localFrictionBefore')
    plaatselijke_wrijving_achteraf = get_ot_none(nulmeting, 'localFrictionAfter')
    waterspanning_u1_vooraf = get_ot_none(nulmeting, 'porePressureU1Before')
    waterspanning_u1_achteraf = get_ot_none(nulmeting, 'porePressureU1After')
    waterspanning_u2_vooraf = get_ot_none(nulmeting, 'porePressureU2Before')
    waterspanning_u2_achteraf = get_ot_none(nulmeting, 'porePressureU2After')
    waterspanning_u3_vooraf = get_ot_none(nulmeting, 'porePressureU3Before')
    waterspanning_u3_achteraf = get_ot_none(nulmeting, 'porePressureU3After')
    cur.execute(query, (i, conusweerstand_vooraf, conusweerstand_achteraf, elektrische_geleidbaarheid_vooraf, elektrische_geleidbaarheid_achteraf,helling_oost_west_vooraf, helling_oost_west_achteraf, helling_noord_zuid_vooraf, helling_noord_zuid_achteraf, hellingresultante_vooraf, hellingresultante_achteraf, plaatselijke_wrijving_vooraf, plaatselijke_wrijving_achteraf, waterspanning_u1_vooraf, waterspanning_u1_achteraf, waterspanning_u2_vooraf, waterspanning_u2_achteraf, waterspanning_u3_vooraf, waterspanning_u3_achteraf))

'''
  Entiteit Bepaalde parameters
'''
def fill_bepaalde_parameters():
  query = '''insert into BepaaldeParameters(fileid, sondeertrajectlengte, diepte, verlopen_tijd, conusweerstand, gecorrigeerde_conusweerstand, 
      netto_conusweerstand, magnetische_veldsterkte_x, magnetische_veldsterkte_y, magnetische_veldsterkte_z, totale_magnetische_veldsterkte,
      elektrische_geleidbaarheid, helling_oost_west, helling_noord_zuid, helling_x, helling_y, hellingresultante, magnetische_inclinatie, magnetische_declinatie,
      plaatselijke_wrijving, porienratio, temperatuur, waterspanning_u1, waterspanning_u2, waterspanning_u3, wrijvingsgetal) 
      values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
  '''
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
    parameters = parser.get_element_from_metadata('parameters')
    sondeertrajectlengte = parameters['penetrationLength']
    diepte = parameters['depth']
    verlopen_tijd = parameters['elapsedTime']
    conusweerstand = parameters['coneResistance']
    gecorrigeerde_conusweerstand = parameters['correctedConeResistance']
    netto_conusweerstand = parameters['netConeResistance']
    magnetische_veldsterkte_x = parameters['magneticFieldStrengthX']
    magnetische_veldsterkte_y = parameters['magneticFieldStrengthY']
    magnetische_veldsterkte_z = parameters['magneticFieldStrengthZ']
    totale_magnetische_veldsterkte = parameters['magneticFieldStrengthTotal']
    elektrische_geleidbaarheid = parameters['electricalConductivity']
    helling_oost_west = parameters['inclinationEW']
    helling_noord_zuid = parameters['inclinationNS']
    helling_x = parameters['inclinationX']
    helling_y = parameters['inclinationY']
    hellingresultante = parameters['inclinationResultant']
    magnetische_inclinatie = parameters['magneticInclination']
    magnetische_declinatie = parameters['magneticDeclination']
    plaatselijke_wrijving = parameters['localFriction']
    porienratio = parameters['poreRatio']
    temperatuur = parameters['temperature']
    waterspanning_u1 = parameters['porePressureU1']
    waterspanning_u2 = parameters['porePressureU2']
    waterspanning_u3 = parameters['porePressureU3']
    wrijvingsgetal = parameters['frictionRatio']
    cur.execute(query, (i,sondeertrajectlengte , diepte, verlopen_tijd, conusweerstand, gecorrigeerde_conusweerstand, netto_conusweerstand, magnetische_veldsterkte_x, magnetische_veldsterkte_y, magnetische_veldsterkte_z, totale_magnetische_veldsterkte, elektrische_geleidbaarheid, helling_oost_west, helling_noord_zuid, helling_x, helling_y, hellingresultante, magnetische_inclinatie, magnetische_declinatie, plaatselijke_wrijving, porienratio, temperatuur, waterspanning_u1, waterspanning_u2, waterspanning_u3, wrijvingsgetal))

'''
  Entiteit Conuspenetratietest
'''
def fill_conepenetratietest():
  query = 'insert into Conuspenetratietest(fileid, starttijd, resulttijd, cptbroid, aantal_metingen) values(%s, %s, %s, %s, %s)'
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i)) 
    cpt = parser.get_element_by_tag_name('conePenetrationTest')
    starttijd = BROXMLParser.get_child_element_by_tag_name(cpt, 'phenomenonTime').text
    resulttijd = BROXMLParser.get_child_element_by_tag_name(cpt,'resultTime').text
    [[_, v]] = cpt.attrib.items()
    cptbroid = v
    aantal_metingen = parser.get_cpt_matrix().shape[0]
    cur.execute(query,(i, starttijd, resulttijd, cptbroid, aantal_metingen))

def fill_dissipationtests():
  query ='insert into Dissipatietest(fileid, sondeertrajectlengte, starttijd, resulttijd, dtbroid, aantal_metingen) values(%s, %s, %s, %s, %s, %s)'
  for i in range(0, NUMBEROFDOCUMENTS):
    if (i % 1000 == 0):
            print("done: %d" % i)
    parser = BROXMLParser.from_file('brofeature%s.xml' % (i)) 
    dissipatietests = parser.get_list_of_elements_by_tag_name('dissipationTest')
    if dissipatietests is not None:
      counter = 0
      diss_matrices = parser.get_diss_matrices()
      for dissip in dissipatietests:
        sondeertrajectlengte = BROXMLParser.get_child_element_by_tag_name(dissip, 'penetrationLength').text
        starttijd = BROXMLParser.get_child_element_by_tag_name(dissip, 'phenomenonTime').text
        resulttijd = BROXMLParser.get_child_element_by_tag_name(dissip, 'resultTime').text
        [[_, v]] = dissip.attrib.items()
        dtbroid = v
        aantal_metingen = diss_matrices[counter].shape[0]
        cur.execute(query,(i, sondeertrajectlengte, starttijd, resulttijd, dtbroid, aantal_metingen))

def get_ot_none(dict, key):
    if (key in dict):
        return dict[key]
    return None

# Run this function to create and fill tables in brocptdatabase
# Adjust the arguments in psycopg2.connect with your db credentials
def go():
  cpts_dir = os.path.join('brocptdata', 'cpts')
  assertDirectoryExists(cpts_dir)
  os.chdir()
  conn = psycopg2.connect(host='localhost',database='brocptdata', user='#', password='#')
  cur = conn.cursor()
  # Database create tables and insert operations
  geotechnisch_sondeeronderzoek()
  fill_geotechnisch_sondeeronderzoek()
  # Geotechnisch sondeeronderzoek done!
  registratiegeschiedenis()
  fill_registratiegeschiedenis()
  # Registratiegeschiedenis done!
  aangeleverde_locatie()
  fill_aangeleverde_locatie()
  # Aangeleverde locatie done!
  gestandaardiseerde_locatie()
  fill_gestandaardiseerde_locatie()
  # Gestandaardiseerde locatie
  sondeonderzoek()
  fill_sondeonderzoek()
  # Sondeonderzoek done!
  traject()
  fill_traject()
  # Traject done!
  bewerking()
  fill_bewerking()
  # Bewerking done!
  sondeerapparaat()
  fill_sondeerapparaat()
  # Sondeerapparaat done!
  bepaalde_parameters()
  fill_bepaalde_parameters()
  # Bepaalde parameters done!
  nulmeting()
  fill_nulmeting()
  # Nulmeting done!
  conuspenetratietest()
  fill_conepenetratietest()
  # Conuspenetratietest done!
  dissipatietest()
  fill_dissipationtests()
  # Dissipatietest done!
  # Finished completed! 
  conn.commit()
  cur.close()
  conn.close()