import operator
import json
import re
from pyproj import Proj, transform
from geocortex import GeoCortex
import os

# 4080 tegels met een oppervlakte van 25km2. De geselecteerde subset bestaat uit 21 tegels
class InitGeoCortex:
    AREA = 5000

    def __init__(self):
        self.geocortexpositions_dict = self.create_geocortex()
        self.result = self.assign_points_to_geocortex(self.geocortexpositions_dict)
     
    def create_geocortex(self):
        cortexpositions_dict = dict()
        x_measures = list()
        y_measures = list()
        for x in range(0, 300000, self.AREA):
            x_measures.append(x)
        for y in range(289000, 629000, self.AREA):
            y_measures.append(y)
        label = 1
        for i in range(len(y_measures)):
            for j in range(len(x_measures)):
                x = (x_measures[j], y_measures[i])
                y = (x_measures[j] + self.AREA, y_measures[i] + self.AREA)
                cortexpositions_dict[label] = GeoCortex(label, x, y)
                label += 1      
        return cortexpositions_dict
    
    def assign_points_to_geocortex(self, geocortexpositions_dict):
        print(os.curdir)
        with open('brocptdata/csv/brolocations.csv', 'r') as f:
            for line in f:
                split = line.split(",")
                docid = int(split[0]) 
                x = float(split[1])
                y = float(split[2])
                epsg = split[3]
                epsg = re.sub('.*EPSG.', 'EPSG', epsg)
                if (epsg != 'EPSG:28992'):
                    inProj = Proj(init= epsg)
                    outProj = Proj(init='EPSG:28992')
                    y, x = transform(inProj,outProj,y,x)
                    self.populate_geocortexes(docid, x, y, geocortexpositions_dict)
        return geocortexpositions_dict

    def populate_geocortexes(self, docid, x, y, geocortexpositions_dict):
        for key, value in geocortexpositions_dict.items():
            if x >= value.start[0] and x < value.end[0] and y >= value.start[1] and y < value.end[1]:
                geocortexpositions_dict[key].add_cpt((docid, x, y))
    
    # Geeft de labels van de top n geocortexes met de meeste CPT's terug 
    def top_geocortexes(self,geocortex_dict, n):
        sorted_cortex_list = sorted(geocortex_dict, key = lambda x: len(geocortex_dict[x].get_cpts()), reverse = True)
        return sorted_cortex_list[:n]
    
    # Tegels selecteren op basis van minimaal en maximaal aantal CPT's 
    def select_geocortexes_range(self, geocortex_dict, min_cpts, max_cpts):
        result = dict()
        for label in geocortex_dict:
            if len(geocortex_dict[label].get_cpts()) >= min_cpts and len(geocortex_dict[label].get_cpts()) < max_cpts:
                result[label] = geocortex_dict[label] 
        return result

    def write_to_file(self, result, outfile):
        with open(outfile, 'w') as f:
            json.dump(result, f)
    
    def load_geocortexes(self, file):
        with open(file, 'r') as json_file:
            data = json.load(json_file)
            return data
    
    def prepare_data_to_draw_cortexonmap(self, file):
        tegels_list = list()
        data = self.load_geocortexes(file)
        for tegel in data:
            label = tegel[0]
            links_rechts = [tegel[1], [tegel[2][0], tegel[2][1] - 5000]]
            links_boven_rechts_boven = [[tegel[1][0], tegel[2][1]], tegel[2]]
            links_boven = [tegel[1], [tegel[1][0], tegel[1][1]+ 5000]]
            rechts_boven = [tegel[2], [tegel[2][0], tegel[2][1] - 5000]]
            tegels_list.append(links_rechts)
            tegels_list.append(links_boven)
            tegels_list.append(rechts_boven)
            tegels_list.append(links_boven_rechts_boven)
        self.write_to_file(tegels_list)

































'''
Start----------------------------------------------------------------------------------------------
'''
# x Cortexes
#print(len(cortex.geocortex_dict))

'''
1301 cortexes of 4080 possesses CPT points
Cortex label 2365 possesses max points; 2330 points
nr1 Cortex position (120000, 484000), (125000, 489000) Amsterdam
# '''

# result_lists = sorted(result, key = lambda x: len(result[x].cpts), reverse = True)
# print(result_lists[0].label)
# print(result_lists[0].cpts)

# max_points_cortex = resulted[0]
# size_of_max_cortex = len(cortexresult_dict[resulted[-1]])
# point= cortexpositions_dict[max_points_cortex]
# # print(max_points_cortex)
# # print(size_of_max_cortex)
# # print(point)
# # print(len(cortexresult_dict[resulted[0]]))
# for line in resulted:
#     print(len(cortexresult_dict[line]), cortexpositions_dict[line] )

# print(len(cortexresult_dict.keys()))
