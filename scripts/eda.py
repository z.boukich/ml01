from scripts.broparser import BROXMLParser
from scripts.brovalidation import assertDirectoryExists
from matplotlib import pyplot as plt
from utils.enums import MISSINGVALUE, NUMBEROFDOCUMENTS
import numpy as np
import pandas as pd
import seaborn as sns
import os
import time
import sys

np.set_printoptions(threshold=sys.maxsize, suppress=True)

#  Ploting features and save to plots folder change the feature if you want to plot other feature 
def plot_all():
    start = time.time()
    cpts_dir = os.path.join('brocptdata', 'cpts')
    assertDirectoryExists(cpts_dir)
    os.chdir(cpts_dir)
    data = list()
    if not os.path.exists('./plots'):
        os.makedirs('./plots')
    for i in range(0, NUMBEROFDOCUMENTS):
        if (i % 1000 == 0):
            print("done: %d" % i)
        parser = BROXMLParser.from_file('brofeature%s.xml' % (i))
        matrix = parser.get_cpt_matrix()
        cleaned_matrix = clean_matrix(matrix, 0, 3)
        x = cleaned_matrix[:,0]
        y = cleaned_matrix[:,3]
        plot_line_graph(x, y, 'Conusweerstand', 'Diepte', 'CPT resultaat file ' + str(i), i)
        researchReportDate = parser.get_element_from_metadata('qualityRegime')
        data.append([researchReportDate])
    df = pd.DataFrame(data, columns = ['Kwaliteitsregime'])
    df['Year'] = (pd.to_datetime(df.Date)).dt.year
    sns.set()
    sns.set_style('whitegrid')
    plt.figure(figsize = (20,15))
    df.Year.value_counts().sort_index().plot()
    plt.xlabel('jaar')
    plt.ylabel('frequentie')
    sns.countplot('Kwaliteitsregime', data = df)
    plt.title('BRO sonderingen | kwaliteitsregime')
    plt.savefig('./plots/kwaliteitsregime.png')
    end = (time.time() - start ) / 60
    print(end)
    
    

# Plot Cpt in a graph for example coneresistance as x vs depth as y to analyse the sort of soil under the ground  
def plot_line_graph(x, y, xlabel, ylabel, title, file_id):
    fig, ax = plt.subplots()  
    ax.plot(y, x)
    plt.gca().invert_yaxis()
    ax.set(xlabel= xlabel, ylabel=ylabel,
        title=title)
    ax.grid()
    fig.savefig('./plots/brofeatures'+ str(file_id) + 'plot.png')
    plt.clf()
    plt.close()

def clean_matrix(matrix, x, y):
    cleaned = matrix[matrix[:,x] != MISSINGVALUE]
    cleaned = cleaned[cleaned[:,y] != MISSINGVALUE]
    cleaned = cleaned[cleaned[:,x].argsort()]
    return cleaned

def indices_of_empty_values(matrix, index):
    result = np.where(matrix[:, index] == -999999)
    return result[0]


def sort_cpt_by_attr(matrix, attr):
    return np.sort(matrix[:, attr])

def diff_of_cptattr(matrix, attr):
    return np.diff(matrix[:, attr])

def create_hist(matrix, attr):
    matrix = matrix[matrix[:, attr] != MISSINGVALUE]
    plt.hist(matrix[:, attr])
    plt.show()

def create_scatter(matrix, x, y):
    matrix = matrix[matrix[:, x] != MISSINGVALUE]
    matrix = matrix[matrix[:, y] != MISSINGVALUE]

    plt.scatter(matrix[:, x],matrix[:, y])
    plt.show()
