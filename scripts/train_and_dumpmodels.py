from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
from sklearn import tree
import numpy as np
import os
import pickle
from sklearn.externals import joblib

NEURAL_NETWORK = "neural_network"
DECISION_TREE = "decision_tree"

if not os.path.exists('finalized'):
    os.makedirs('finalized')
    print("finalized folder created")

def neural_network_model(x_train, y_train):
    mlp = MLPClassifier(verbose=True)
    mlp.fit(x_train,y_train)
    dump_model(NEURAL_NETWORK, mlp)

def decision_tree_model(x_train, y_train):
    decision_tree = tree.DecisionTreeClassifier()
    decision_tree.fit(x_train, y_train)
    dump_model(DECISION_TREE, decision_tree)

def dump_model(name, model):
    filename = 'finalized/finalized_model_'+ name +'.sav'
    pickle.dump(model, open(filename, 'wb'))

def dump_models():
    train_data = np.genfromtxt(os.path.join('brocptdata', 'sets', 'train.data'), delimiter=',')
    test_data = np.genfromtxt(os.path.join('brocptdata', 'sets', 'test.data'), delimiter=',')
    X_train, _, y_train, _ = train_test_split_cpt(train_data, test_data)
    neural_network_model(X_train, y_train)
    print("Neural network done")

def train_test_split_cpt(train_data, test_data):
    X_train = train_data[ :, : -1 ]
    y_train = train_data[ : , -1 ]
    X_test = test_data[ :, : -1 ]
    y_test = test_data[ : , -1 ]

    #Normalize the data 
    train_set = np.concatenate((X_train, X_test), axis=0)
    scaler = StandardScaler()
    X_scaled = scaler.fit(train_set).transform(train_set)
    X_train = X_scaled[0: X_train.shape[0]]
    X_test = X_scaled[X_train.shape[0]:]

    return X_train, X_test, y_train, y_test 

# Check performance of the model 
def evaluate_model(model):
    mlp = joblib.load("finalized/finalized_model_" + model + ".sav")
    train_data = np.genfromtxt(os.path.join('brocptdata', 'sets', 'train.data'), delimiter=',')
    test_data = np.genfromtxt(os.path.join('brocptdata', 'sets', 'test.data'), delimiter=',')
    
    _, X_test, _, y_test = train_test_split_cpt(train_data, test_data)

    print('Accuracy on the test subset: {:.3f}'.format(mlp.score(X_test, y_test)))