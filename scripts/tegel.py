
class Tegel:
    def __init__(self, label, start, end):
        self.label = label
        self.start = start
        self.end = end
        self.cpt = list()

    def add_cpt(self, cpt):
        self.cpt.append(cpt)

    def get_cpts(self):
        return self.cpt
