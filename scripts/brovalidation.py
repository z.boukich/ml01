import os

def assertFileExists(filename):
    if not os.path.isfile(filename):
        raise Exception("file does not exist: %s" % filename)

def assertDirectoryExists(dir):
    if not os.path.isdir(dir):
        raise Exception("Directory does not exist: %s" % dir)
