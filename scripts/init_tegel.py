import json
import re
from pyproj import Proj, transform
from scripts.tegel import Tegel
import pandas as pd

# 4080 tegels met een oppervlakte van 25km2. De geselecteerde subset bestaat uit 21 tegels
class InitTegel:
    AREA = 5000

    def __init__(self):
        self.tegelpositions_dict = self.create_tegel()
        self.result = self.assign_points_to_tegel(self.tegelpositions_dict)
     
    def create_tegel(self):
        tegelpositions_dict = dict()
        x_measures = list()
        y_measures = list()
        for x in range(0, 300000, self.AREA):
            x_measures.append(x)
        for y in range(289000, 629000, self.AREA):
            y_measures.append(y)
        label = 1
        for i in range(len(y_measures)):
            for j in range(len(x_measures)):
                x = (x_measures[j], y_measures[i])
                y = (x_measures[j] + self.AREA, y_measures[i] + self.AREA)
                tegelpositions_dict[label] = Tegel(label, x, y)
                label += 1      
        return tegelpositions_dict
    
    def assign_points_to_tegel(self, tegelpositions_dict):
        with open('brocptdata/csv/brolocations.csv', 'r') as f:
            for line in f:
                split = line.split(",")
                docid = int(split[0]) 
                x = float(split[1])
                y = float(split[2])
                epsg = split[3]
                epsg = re.sub('.*EPSG.', 'EPSG', epsg)
                if (epsg != 'EPSG:28992'):
                    inProj = Proj(init= epsg)
                    outProj = Proj(init='EPSG:28992')
                    y, x = transform(inProj,outProj,y,x)
                    self.populate_tegels(docid, x, y, tegelpositions_dict)
        return tegelpositions_dict

    def populate_tegels(self, docid, x, y, tegelpositions_dict):
        for key, value in tegelpositions_dict.items():
            if x >= value.start[0] and x < value.end[0] and y >= value.start[1] and y < value.end[1]:
                tegelpositions_dict[key].add_cpt((docid, x, y))
    
    # Geeft de labels van de top n geocortexes met de meeste CPT's terug 
    def top_tegels(self,tegel_dict, n):
        sorted_tegel_list = sorted(tegel_dict, key = lambda x: len(tegel_dict[x].get_cpts()), reverse = True)
        return sorted_tegel_list[:n]
    
    # Tegels selecteren op basis van minimaal en maximaal aantal CPT's 
    def select_tegels_range(self, tegel_dict, min_cpts, max_cpts):
        result = dict()
        for label in tegel_dict:
            if len(tegel_dict[label].get_cpts()) >= min_cpts and len(tegel_dict[label].get_cpts()) < max_cpts:
                result[label] = tegel_dict[label] 
        return result

    def write_to_file(self, result, outfile):
        with open(outfile, 'w') as f:
            json.dump(result, f)
    
    def load_tegels(self, file):
        with open(file, 'r') as json_file:
            data = json.load(json_file)
            return data
    
    def prepare_data_to_draw_tegelonmap(self, file):
        tegels_list = list()
        data = self.load_geocortexes(file)
        for tegel in data:
            label = tegel[0]
            links_rechts = [tegel[1], [tegel[2][0], tegel[2][1] - 5000]]
            links_boven_rechts_boven = [[tegel[1][0], tegel[2][1]], tegel[2]]
            links_boven = [tegel[1], [tegel[1][0], tegel[1][1]+ 5000]]
            rechts_boven = [tegel[2], [tegel[2][0], tegel[2][1] - 5000]]
            tegels_list.append(links_rechts)
            tegels_list.append(links_boven)
            tegels_list.append(rechts_boven)
            tegels_list.append(links_boven_rechts_boven)
        self.write_to_file(tegels_list, 'tegels.data')

