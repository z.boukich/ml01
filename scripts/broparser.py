from utils.enums import CPTParams, MISSINGVALUE, CPTPARAMSLENGTH, DISSPARAMSLENGTH
from lxml import etree
import numpy as np
import pandas as pd
from scripts.brovalidation import assertFileExists 
import os
import re
import sys
np.set_printoptions(threshold=sys.maxsize, suppress=True) 

# Parser to retrieve CPT elements
class BROXMLParser:

    def __init__(self, xml_tree):
        self.data_dir = os.path.abspath(os.curdir)
        self.xml_tree = xml_tree
        self.metadata = BROXMLParser.lxml_to_dict(xml_tree.getroot())
        self.cpt_matrix = self.generate_cpt_matrix()
        self.diss_matrices = self.generate_dissipation_matrix()
    
    @staticmethod
    def lxml_to_dict(element):
        result = dict()
        if element.getchildren() == []:
            tag = BROXMLParser.strip_ns_from_tagname(element.tag)
            result[tag] = element.text
        else:
            count = dict()
            for elem in element.getchildren():
                subdict = BROXMLParser.lxml_to_dict(elem)
                tag = BROXMLParser.strip_ns_from_tagname(element.tag)
                sub_tag = BROXMLParser.strip_ns_from_tagname(elem.tag)   
                if sub_tag is None:
                    continue
                # A CPT(sondering) might have zero or more Dissipationtests
                # So this adds a number at the end of the name to differentiate between the dissipationtests
                # dissipationtest1, dissipationtest2 etc
                if result.get(tag, False) and sub_tag in result[tag].keys():
                    count[sub_tag] = count[sub_tag]+1 if count.get(sub_tag, False) else 1
                    elemtag = sub_tag + str(count[sub_tag])
                    subdict = {elemtag: subdict[sub_tag]}
                if result.get(tag, False):
                    result[tag].update(subdict)
                else:
                    result[tag] = subdict          
        return result

    def get_element_from_metadata(self, name, d = None):
        if (d is None):
            d = self.metadata
        for (key, item) in d.items():
            if (key == name):
                return item
            if (isinstance(item, dict)):
                optional = self.get_element_from_metadata(name, item)
                if (optional is not None):
                    return optional
        return None   

    @staticmethod
    def strip_ns_from_tagname(name):
        return re.sub('{.*}', '', name)
    
    def generate_cpt_matrix(self):
        cpt_result = self.get_element_from_metadata('cptResult')
        if cpt_result == None:
            return None
        return BROXMLParser.convert_data_to_matrix(cpt_result['values'], CPTPARAMSLENGTH)  
    
    def generate_dissipation_matrix(self):
        dissipation_tests = self.get_list_of_elements_by_tag_name('dissipationTest')
        if dissipation_tests == None:
            return None
        diss_matrix_list = list()
        for dissipation_test in dissipation_tests:
            values = self.get_child_element_by_tag_name(dissipation_test, 'values')
            matrix = BROXMLParser.convert_data_to_matrix(values.text, DISSPARAMSLENGTH)
            diss_matrix_list.append(matrix)
        return diss_matrix_list

    @staticmethod
    def convert_data_to_matrix(data, rows):
        lines = data.split(';')
        lines = [line.split(',') for line in lines]
        lines = [ line for line in lines if len(line) == rows ]
        return np.array(lines, dtype=np.float)

    def get_cpt_matrix(self):
        return self.cpt_matrix
    
    def get_diss_matrices(self):
        return self.diss_matrices

    def get_location(self, name):
        location = self.get_element_by_tag_name(name)
        if (location is not None):
            loc = BROXMLParser.get_child_element_by_tag_name(location, 'location')
            if loc is not None:
                pos = BROXMLParser.get_child_element_by_tag_name(location, 'pos')
                srs = BROXMLParser.get_child_element_by_tag_name(location, 'Point')
                x, y = pos.text.split(" ")
                srs = srs.attrib['srsName']
                return (srs , float(x), float(y))
        return None

    def get_element_by_tag_name(self, name):
        for _, elem in etree.iterwalk(self.xml_tree, events = ('end',)):
            tag = re.sub('{.*}', '', elem.tag)
            if (name == tag):
                return elem
        return None

    def get_list_of_elements_by_tag_name(self, name):
        list_of_elements = list()
        for _, elem in etree.iterwalk(self.xml_tree, events = ('end',)):
            if (name == BROXMLParser.strip_ns_from_tagname(elem.tag)):
                list_of_elements.append(elem)
        if (len(list_of_elements) != 0):
            return list_of_elements
        return None

    @staticmethod
    def get_child_element_by_tag_name(elem, name):
        for _, elem in etree.iterwalk(elem, events = ('end',)):
            tag = re.sub('{.*}', '', elem.tag)
            if (name == tag):
                return elem
        return None

    @staticmethod
    def generate_dataframe_from_cptfile(csvfile, col_names):    
        df = pd.read_csv(csvfile, header= None, names= col_names, dtype=float)
        return df

    @classmethod
    def from_file(cls, xml_file):
        assertFileExists(xml_file)
        return cls(etree.parse(xml_file))

    def get_brofeature_filename(self, i):
        return self.data_dir + '\\brofeature' + str(i) +'.xml'
    
    def get_brofeature_file_id(self, filename):
        return ''.join(re.findall(r'\d', filename))