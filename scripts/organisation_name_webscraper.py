import urllib.request
from bs4 import BeautifulSoup
import psycopg2
from scripts.broparser import BROXMLParser

URL = 'https://www.broinfo.nl/aangemeld-bij-de-bro'

# Download html
def download_html(url):
    try:
        response = urllib.request.urlopen(url)
        return response.read()
    except Exception as e:
        print('Exception occured during urlrequest: %s  : %s' % (url, str(e)))
    
def kvk_bronhouders(html):
    soup = BeautifulSoup(html, features="lxml")
    p = soup.select_one('div > p:nth-of-type(3)').get_text()

# Create table bronhouder   
def create_table_bronhouder():
    cur.execute('''CREATE TABLE bronhouder
      (KVK TEXT NOT NULL,
      naam TEXT NOT NULL,
      aangemeld TEXT NOT NULL);''')
    print ("Table created successfully")

# Fill table bronhouder
def fill_table_bronhouder(lines):
    lines = lines.splitlines()
    for i in range(len(lines)):
        line = lines[i].split('|')
        if len(line) >= 2:
            name = line[0]
            kvk = line[1]
        aangemeld = 'nee'
        if '(B)' in kvk or '(\u200bB)' in kvk:
            aangemeld = 'ja'
            kvk = kvk.split()[0]
        cur.execute('insert into bronhouder(kvk, naam, aangemeld) values(%s, %s, %s)', (kvk, name, aangemeld))
      