from scripts.broparser import BROXMLParser
from utils.enums import CPTParams, MISSINGVALUE, CPTPARAMSLENGTH
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import re
import os
import json
import time
import pandas as pd

FEATURES =[CPTParams.PENETRATIONLENGTH.value[0], CPTParams.CONERESISTANCE.value[0], CPTParams.LOCALFRICTION.value[0], CPTParams.FRICTIONRATIO.value[0]]
TEST_SIZE = 0.2
TRAIN_TEST_CPT_DICT = dict()
min_max_dict = {"Sondeertrajectlengte" : {"min": 0, "max":200}, "Conusweerstand":{"min": -1, "max": 200}, "Plaatselijke_wrijving": {"min": -1, "max": 200},
 "Wrijvingsgetal":{"min":0, "max":200},"Verschuiving": {"min": -200, "max": 200}, "Conuspunt_oppervlakte": {"min":25, "max": 2000}}

''' Generate a train and test set from each tile then sum the training sets into one train.data file and testing sets into one test.data file.
Test file is the hold out set and used to simulate out of sample data to get a more reliable estimate
'''

#Clean the data from measures where there is no coneresistance and penetrationlength measures 
def prepare_train_test_data():
    os.chdir(os.path.join('brocptdata', 'sets'))
    for dir in os.listdir():
        file_list = list()
        for f in os.listdir(dir):
            file_list.append(f)
        train_set, test_set = train_test_split(file_list, test_size = TEST_SIZE)
        TRAIN_TEST_CPT_DICT[dir] = (train_set, test_set)
        write(train_set, dir, 'final_train.data')
        write(test_set, dir, 'final_test.data') 

# Clean data, select relevant features, normalize and write it to the outfile
# Features are BROID, Sondeertrajectlengte, Conusweerstand, Plaatselijke wrijving, Wrijvingsgetal, Verschuiving, Oppervlakte conuspunt, Tegellabel 
def write(dataset, label, out_file):
    for cptfile in dataset:
        parser = BROXMLParser.from_file(os.path.join(label, cptfile))
        matrix = parser.get_cpt_matrix() 
        temp = matrix[:, FEATURES]
        feature_matrix = np.zeros((temp.shape[0], temp.shape[1]+3))
        feature_matrix[:, 0:4] = temp
        # Add features
        offset = float(parser.get_element_from_metadata('offset'))
        # cone_surface_arae = parser.get_element_from_metadata('coneSurfaceArea')
        bro_id = parser.get_element_from_metadata('broId')
        bro_id = re.sub('CPT', '', bro_id)
        feature_matrix[:, 4] = offset
        # feature_matrix[:, 5] = cone_surface_arae
        feature_matrix[:, 5] = bro_id
        feature_matrix[:, 6] = label
        # Clean data, remove irrelevant observations
        mask = np.any(np.isnan(feature_matrix) | np.equal(feature_matrix, MISSINGVALUE), axis=1)
        cleaned_matrix = feature_matrix[~mask]
        # Normalize data
        nrows = cleaned_matrix.shape[0]
        nrows =0
        # feature_matrix = cleaned_matrix
        if nrows > 0:
                feature_matrix[:, 0 ] = np.apply_along_axis(normalize_feature, 1, cleaned_matrix[:, 0].reshape((nrows, 1)), 'Sondeertrajectlengte').reshape((nrows))
                feature_matrix[:, 1 ] = np.apply_along_axis(normalize_feature, 1, cleaned_matrix[:, 1].reshape((nrows, 1)), 'Conusweerstand').reshape((nrows))
                feature_matrix[:, 2 ] = np.apply_along_axis(normalize_feature, 1, cleaned_matrix[:, 2].reshape(nrows, 1), 'Plaatselijke_wrijving').reshape((nrows))
                feature_matrix[:, 3 ] = np.apply_along_axis(normalize_feature, 1, cleaned_matrix[:, 3].reshape(nrows, 1), 'Wrijvingsgetal').reshape((nrows))
                feature_matrix[:, 4] = normalize_feature(float(offset), 'Verschuiving')
                # feature_matrix[:, 5] = normalize_feature(float(cone_surface_arae), 'Conuspunt_oppervlakte')
                f = open(out_file, "a")
                np.savetxt(f, feature_matrix)
        f = open(out_file, "a")
        np.savetxt(f, feature_matrix)

# Normalize values into range (-1, 1)
def normalize_feature(value, feature):
        return 2 * ((value - min_max_dict[feature]['min']) / (min_max_dict[feature]['max'] - min_max_dict[feature]['min'])) -1

def denormalize_feature(value, feature):
        return ((value + 1) / 2) * (min_max_dict[feature]['max'] - min_max_dict[feature]['min']) + min_max_dict[feature]['min']


